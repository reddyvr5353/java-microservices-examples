#! /bin/bash
folder=`git diff --name-only --diff-filter=ADMR @~..@ | awk 'BEGIN {FS="/"} {print $1}' | uniq`
echo $folder
if [ "$folder" = "jhipster" ]
then
cd jhipster/blog/src/test
ls
else
cd "$folder"
ls
fi
pwd
cd jhipster
chmod 777 build.sh
./build.sh